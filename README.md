# Network shared folder

Share a folder WebDAV, ready to be mounted as a network drive on Windows.

The following images are generated:

- Production registry (stable images, persistent, immutable)
  - **latest**: Last tagged image, ensures a tested and working condition
    - FQN: `registry.gitlab.com/woodswallow/hub/net-share:latest`
- Development registry (latest images, maybe inconsistent, maybe not immutable)
  - **master**: Last image from master branch, assumes a working condition
    - FQN: `registry.gitlab.com/woodswallow/oci/net-share:master`

To connect using WebDAV:

```bash
docker run \
  --rm \
  -it \
  -p 127.0.0.1:20171:80/tcp \
  registry.gitlab.com/woodswallow/hub/net-share:latest
```

Then map a network drive to `127.0.0.1:20171` by following Windows indications.

## Windows users

Due to limitations found on Docker Desktop for Windows, it is required that the
user manually login and pull the images using the command-line, as at the time
of this writing, there is no way to make Docker Desktop to login and pull from
private registries.

To do so, first ensure Docker for Windows is correctly installed and configured
with WSL2 backend (tested on Ubuntu 20.04 WSL2 version).

### Pull the image

Then, open a WSL2 terminal and login + pull the image as follows:

```bash
docker login registry.gitlab.com
docker pull registry.gitlab.com/woodswallow/hub/net-share:latest
```

Which should print something like the following picture (running the image from
command-line is optional, and shown here for demonstrative purposes only):

![Figure showing an example of logging in and pulling from a private registry](./docs/01_docker-cli_login-pull-run.png)

### Use the image

From a Windows + Docker Desktop perspective, the simpler the better, so the way
it is going to be used will include not deleting the image, so the user can run
or stop the image at will.

> **NOTE:** Due to Docker Desktop for Windows limitations, any `pull` command
> has to be done using the command-line, mandatory.

Once the image is pulled, run a container from it as follows:

![Run container button](./docs/02_docker-gui_run.png)

Configure container details, set some name (not used anywhere yet), and ensure
to map a host port to the container port. In this example TCP port `20171` will
be mapped to internal TCP port `80`:

![Set mapped port to be used to connect to the share](./docs/03_docker-gui_configure-container.png)

If everything went fine, somethin like the following figure should be shown:

![Correctly running container](./docs/04_docker-gui_running-container.png)

If clicking the container row, a detailed overview appears where some tools for
monitoring and operating with the container are available:

![Container detailed view, showing stdout output](./docs/05_docker-gui_container-stdout-and-toolbar.png)

To monitor the current resources usage of the container, click 'STATS':

![Container stats](./docs/06_docker-gui_container-stats.png)

To launch a browser to preview/navigate the shared content, click the first icon
at 'operations' group, then something like the following should appear:

![Accessing drive content using a browser](./docs/07_browser_html-view.png)

In order to access the content in an easy way, it is possible to mount it as a
mapped network drive (with its own letter). To do so, right-click on 'My PC' and
click the 'Map network drive...' option:

![Map drive option](./docs/08_explorer_my-pc-map-network-drive.png)

The next is to configure the address the same as shown in the browser, which in
this example is `http://localhost:20171`:

![URL to map the drive to, using previously mapped port](./docs/09_explorer_network-drive-url.png)

Once clicked on 'Finish' a new explorer window should appear with the mapped
drive and the content shown on screen. At this point, it is possible to work
with the files as any other drive:

![Mapped drive showing its content](./docs/10_explorer_mapped-drive-content.png)

As a performance demonstration, the following picture shows how a file of 4 Gb
is copied into the drive. The high speed demonstrates the limit is set by the
SSD, not the microservice:

![Example of a big file copy (SSD speed)](./docs/11_explorer_copy-big-file.png)
