# Base image to be used
ARG OCI_FROM_IMAGE_FQN="docker.io/library/alpine:3.13"
FROM ${OCI_FROM_IMAGE_FQN}

# From: https://github.com/nginxinc/docker-nginx/blob/master/stable/alpine-perl
#
# User/group: nginx(101)/nginx(101)
#
# Any script to be executed during 'entrypoint' stage has to have executable
# bit and has to be placed at '/docker-entrypoint.d' folder. Official nginx
# image already contains 3 files, from 10 to 30, so a good start point is 40.
#
# COPY 40-my-custom-entrypoint.sh /docker-entrypoint.d
#
# If not replaced, original CMD read as follows:
#
# CMD ["nginx", "-g", "daemon off;"]
#
# The way the configuration of the WebDAV is done is by following the guide at:
# https://www.robpeck.com/2020/06/making-webdav-actually-work-on-nginx/

# Default user and group identifiers (already created in nginx-alpine)
ARG OCI_USER_UID="101"
ARG OCI_USER_GID="101"

# Install block
RUN set -ex; \
  # create nginx user/group first, to be consistent throughout docker variants
  addgroup \
    -g ${OCI_USER_GID} \
    -S nginx; \
  adduser \
    -S \
    -D \
    -H \
    -u ${OCI_USER_UID} \
    -h /var/cache/nginx \
    -s /sbin/nologin \
    -G nginx \
    -g nginx \
    nginx; \
  # Update packages
  apk update; \
  apk upgrade; \
  # Install nginx and modules
  apk add --no-cache \
    nginx \
    nginx-mod-http-headers-more \
    nginx-mod-http-dav-ext \
    nginx-mod-http-fancyindex \
  ; \
  # Bring in gettext so we can get `envsubst`, then throw
  # the rest away. To do this, we need to install `gettext`
  # then move `envsubst` out of the way so `gettext` can
  # be deleted completely, then move `envsubst` back.
  apk add --no-cache --virtual .gettext gettext; \
  mv /usr/bin/envsubst /tmp/; \
  runDeps="$( scanelf --needed --nobanner /tmp/envsubst \
    | awk '{ gsub(/,/, "\nso:", $2); print "so:" $2 }' \
    | sort -u \
    | xargs -r apk info --installed \
    | sort -u \
  )"; \
  apk add --no-cache $runDeps; \
  apk del .gettext; \
  mv /tmp/envsubst /usr/local/bin/; \
  # Bring in tzdata so users could set the timezones through the environment
  # variables
  apk add --no-cache tzdata; \
  # Bring in curl and ca-certificates to make registering on DNS SD easier
  apk add --no-cache curl ca-certificates; \
  # forward request and error logs to docker log collector
  ln -sf /dev/stdout /var/log/nginx/access.log; \
  ln -sf /dev/stderr /var/log/nginx/error.log; \
  # create a docker-entrypoint.d directory and webdav folder
  mkdir -pv \
    /docker-entrypoint.d \
    /net-share; \
  # add foo content
  echo "itneic o.O" > /net-share/foo.txt; \
  # fix permissions
  chown -R nginx:nginx /net-share; \
  # Clean stage
  rm -rf /var/cache/apk/*;

ENV \
  # Non-privileged user inside container to be used
  OCI_USER_UID="${OCI_USER_UID}" \
  OCI_USER_GID="${OCI_USER_GID}" \
  # From official nginx docker image
  NGINX_VERSION=1.20.0 \
  NJS_VERSION=0.5.3 \
  PKG_RELEASE=1

COPY files/docker-entrypoint.sh /
COPY files/10-listen-on-ipv6-by-default.sh /docker-entrypoint.d
COPY files/20-envsubst-on-templates.sh /docker-entrypoint.d
COPY files/30-tune-worker-processes.sh /docker-entrypoint.d

ENTRYPOINT ["/docker-entrypoint.sh"]

# Ports to be used
EXPOSE \
  "80/tcp"

STOPSIGNAL SIGQUIT

CMD ["nginx", "-g", "daemon off;"]

# Customizations over the generic installation
COPY files/nginx.conf /etc/nginx
COPY files/default.conf /etc/nginx/conf.d
COPY files/fix_file-size-limit.reg /net-share
