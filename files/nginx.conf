# File taken from the original image, then edited accordingly
user  nginx;
worker_processes  auto;

error_log  /var/log/nginx/error.log notice;
pid        /var/run/nginx.pid;

load_module "modules/ngx_http_dav_ext_module.so";
load_module "modules/ngx_http_fancyindex_module.so";
load_module "modules/ngx_http_headers_more_filter_module.so";

events {
    worker_connections  1024;
}

http {
    include       /etc/nginx/mime.types;
    default_type  application/octet-stream;

    log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
                      '$status $body_bytes_sent "$http_referer" '
                      '"$http_user_agent" "$http_x_forwarded_for"';

    access_log  /var/log/nginx/access.log  main;

    # Setting the 3 ON: ensure packages are full before sending them to the
    # client and, for the last packet, tcp_nopush will be removed, allowing TCP
    # to send it immediately, without the 200ms delay
    sendfile    on;
    tcp_nopush  on;
    tcp_nodelay on;

    send_timeout 3600;
    client_body_timeout 3600;
    keepalive_timeout 3600;
    lingering_timeout 3600;
    client_max_body_size 10G;
    dav_ext_lock_zone zone=foo:10m;

    # May be affected by BREACH attack, but we don't care on our local usage
    gzip  on;

    include /etc/nginx/conf.d/*.conf;
}
